local common = require("common")

local OUTPUT_MODE_NONE = "none"
local OUTPUT_MODE_STATUS_AREA = "status"
local OUTPUT_MODE_SOCKET = "socket"
local extra_output_mode = OUTPUT_MODE_NONE


local aoc_directions_from_square = {
    {name="Abillity Trainers         ", directions="2e4n2e1s1e1s"},
    {name="Food                      ", directions="1W1N"},
    {name="Donation Room             ", directions="N2W"},
    {name="Weapon Shop               ", directions="3E1S1E"},
    {name="Armor Shop                ", directions="1W1S1W"},
    {name="Misc. Items Shop          ", directions="Enter Tent, E"},
    {name="Scroll Shop               ", directions="2W2NE"},
    {name="General Store             ", directions="1W3S1E"},
    {name="Inn                       ", directions="1E1N"},
    {name="Sheltered Courtyard       ", directions="1E1N1W"},
    {name="Golem Tower               ", directions="4E2N"},
    {name="Surai's House of Spirits  ", directions="4E2S1E"},
    {name="Jade Rogue Inn            ", directions="3W3S1E"},
    {name="Stables                   ", directions="1E1S1W1S"}
}

local function useful_dirs()
    blight.output("Useful Directions:")
    for i, val in ipairs(aoc_directions_from_square) do
        blight.output(i .. ")\t" .. val.name .. " - " .. val.directions)
    end
    blight.output("type `goto #` replacing the # with a number from the list")
end

local function goto_common_place(match)
    local dir_index = tonumber(match[2])
    local dir = aoc_directions_from_square[dir_index]
    blight.output("Going to " .. dir.name)
    common.multi_move(dir.directions)
 end
 
alias.add("^printdirs$", useful_dirs)
alias.add("^goto$", useful_dirs)
alias.add("^goto\\s+(\\d\\d?)$", goto_common_place)



local haste_ended = "^Your body and mind return to their normal speed.$"
trigger.add(haste_ended, {}, function()
    common.weave_me("haste")
end)


local function init_exit_info() 
    return {
        north="",
        south="",
        east="",
        west="",
        up="",
        down=""
    }
end

local exit_info = init_exit_info()

local function reset_exit_info()
    exit_info = init_exit_info()
end

local function set_direction(direction, description)
    exit_info[direction] = description
end

local function set_north(descrip)
    set_direction("north", descrip)
end

local function set_south(descrip)
    set_direction("south", descrip)
end

local function set_east(descrip)
    set_direction("east", descrip)
end

local function set_west(descrip)
    set_direction("west", descrip)
end

local function set_up(descrip)
    set_direction("up", descrip)
end

local function set_down(descrip)
    set_direction("down", descrip)
end



local exits_matches = {
    {key="Obvious exits:", func=reset_exit_info},
    {key="North - ", func=set_north},
    {key="South - ", func=set_south},
    {key="East  - ", func=set_east},
    {key="West  - ", func=set_west},
    {key="Up    - ", func=set_up},
    {key="Down  - ", func=set_down},
}



local function print_exit_info()
    local direction_top_line = cformat("<black>____________________<blue>N: <white>" .. exit_info.north .. "<black>__________<green>U: <yellow>" .. exit_info.up .. "<reset>")
    local direction_middle_line = cformat("<blue>W: <white>" .. exit_info.west .. "<black>________________________<blue>E: <white>" .. exit_info.east .. "<reset>")
    local direction_bottom_line = cformat("<black>____________________<blue>S: <white>" .. exit_info.south .. "<black>__________<green>D: <yellow>" .. exit_info.down .. "<reset>")
    
    if extra_output_mode == OUTPUT_MODE_STATUS_AREA then
        blight.status_line(1, direction_top_line)
        blight.status_line(2, direction_middle_line)
        blight.status_line(3, direction_bottom_line)
    elseif extra_output_mode == OUTPUT_MODE_SOCKET  then
        common.clear_1234()
        common.send_to_1234(direction_top_line)
        common.send_to_1234(direction_middle_line)
        common.send_to_1234(direction_bottom_line)
    end
end

local print_task = false
local function print_output()
    if not print_task then
        print_task = true
        print_task = tasks.spawn_later(0.05, function()
            print_exit_info()
            print_task = false
        end)
    end
end

local exit_re = regex.new("((Obvious exits:)($))|(((^North - )|(^South - )|(^East  - )|(^West  - )|(^Up    - )|(^Down  - ))(.*)$)")
local function handle_exits(line)
    if extra_output_mode == OUTPUT_MODE_NONE then
        return line
    end
    local line_str = line:line()
    local match = exit_re:match(line_str)
    if match then
        line:gag(true)
        local left = match[1]
        if left ~= "Obvious exits:"  then
            left = match[6]
        end
        right = match[13]
        for i, val in ipairs(exits_matches) do
            if left == val.key then
                val.func(right)
                break
            end
        end
        print_output()
    end
    return line
end




mud.add_output_listener(handle_exits)



alias.add("^output mode (" .. OUTPUT_MODE_NONE .. "|" .. OUTPUT_MODE_STATUS_AREA .. "|" .. OUTPUT_MODE_SOCKET .. ")", function(match)
    extra_output_mode = match[2]
    blight.output("Changing output mode to " .. extra_output_mode)
    if extra_output_mode == OUTPUT_MODE_NONE then
        blight.status_height(0)
    elseif extra_output_mode == OUTPUT_MODE_STATUS_AREA then
        blight.status_height(5)
    elseif extra_output_mode == OUTPUT_MODE_SOCKET then
        blight.status_height(0)
        blight.output("Make sure you have a server listening to port 1234:")
        blight.output(cformat("<red>nc -lkp 1234<reset>"))
    end
end)

alias.add(common.multi_move_alias_pattern, function(matches)
    common.multi_move(matches[1], nil, function(dirs)
        if extra_output_mode ~= OUTPUT_MODE_NONE then
            mud.send("exits")
        end
    end)
end)

blight.output(C_RED .. "AOC Scripts Loaded" .. C_RESET)


