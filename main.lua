blight.output("mjrbs loading...")

-- blight.status_line(1, "TESTING TESTING")

local function reload_scripts()
    blight.status_height(1)
    blight.status_line(0, "")
    script.reset()
    script.load("~/.config/blightmud/config.lua")
end

local self = {
    host = store.session_read("cur_host"),
    port = tonumber(store.session_read("cur_port") or "0"),
}


local function disconnect()
    self.host = nil
    self.port = nil
    store.session_write("cur_host", tostring(nil))
    store.session_write("cur_port", tostring(nil))
    reload_scripts()
end

local function on_connect(host, port)
    store.session_write("cur_host", host)
    store.session_write("cur_port", tostring(port))

    if host == "aoc.pandapub.com" then
        blight.output(C_RED .. "Loading AOC Scripts" .. C_RESET)
        require("aoc")
    else
        blight.output("unknown mud. no special scripts")
    end
end

mud.on_connect(function (host, port)
        on_connect(host, port)
end)

mud.on_disconnect(function ()
        disconnect()
end)

if self.host and self.port then
        on_connect(self.host, self.port)
end

local function connect_to_aoc()
    mud.connect("aoc.pandapub.com", 4000)

end

alias.add("^reload$", reload_scripts)
alias.add("^aoc$", connect_to_aoc)

alias.add("^test$", function ()
    blight.output("THE TEST ALIAS WAS TRIGGERED")
end)


msdp.on_ready(function()
    blight.output("MSDP READY")
end)