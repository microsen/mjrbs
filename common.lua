local common = {}

local function get_sock_1234()
    return socket.connect("localhost", 1234) --  nc -lkp 1234
end

function common.clear_1234(msg)
    local conn = get_sock_1234()
    if conn then
        conn:send("\x1b[2J\x1b[1;1H") -- Clear the screen and reset cursor to top right
        conn:close()
        return true
    else
        return false
    end
end

function common.send_to_1234(msg)
    local conn = get_sock_1234()
    if conn then
        conn:send(msg) -- Print some formatted text
        conn:send("\n")
        conn:close()
        return true
    else
        return false
    end
end

function common.send_n_times(msg, times)
    for i=1, times, 1 do mud.send(msg) end
end


local multi_move_re = regex.new("([0-9]*)([eEnNwWsSuUdD])")
function common.multi_move(move_str, callback_per_move, callback_finished)
    print("Moving ", move_str)
    -- common.send_to_1234(cformat("\n<red>Moving " .. move_str .. "<reset>"))

    local matches = multi_move_re:match_all(move_str)
    for index, value in pairs(matches) do
        local count = value[2] 
        -- if no count it should be 1
        if count == "" then count = 1 end 
        local direction = value[3]
        common.send_n_times(direction, count)
        if callback_per_move then
            for i = 1, count do
                callback_per_move(direction) 
            end
        end
    end
    if callback_finished then
        callback_finished(move_str)
    end
end

function common.weave_me(spell)
    mud.send("weave '" .. spell .. "' me" )
end

local function multi_move_match(matches)
    common.multi_move(matches[1])
end

common.multi_move_alias_pattern = "^([0-9]*[eEnNwWsSuUdD])+$"
-- alias.add(common.multi_move_alias_pattern, multi_move_match)


local function send_n_times_match(match)
   local count = match[2]
   local cmd = match[3]
   common.send_n_times(cmd, count)
end
alias.add("^([0-9])x (.*)$", send_n_times_match)


local multi_command_alias_pattern = "^.+;.*$"
function common.multi_command(command_str)
    for cmd in string.gmatch(command_str, "([^;]+)") do
        mud.send(cmd)
    end
end
alias.add(multi_command_alias_pattern, function(match)
    common.multi_command(match[1])
end)


return common