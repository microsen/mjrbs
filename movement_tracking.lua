local move_data = {move_stack={}, pos_stack={}, cur_pos={x=0,y=0,z=0}} -- x: N/S, y: E/W, z: U/D

local move_deltas = {
    n = {x=1,y=0,z=0},
    s = {x=-1,y=0,z=0},
    e = {x=0,y=1,z=0},
    w = {x=0,y=-1,z=0},
    u = {x=0,y=0,z=1},
    d = {x=0,y=0,z=-1}
}

function move_data.log_move(dir)
    dir = string.lower(dir)
    local delta = move_deltas[dir]
    local current = move_data.cur_pos
    local new_pos = {
        x=current.x + delta.x,
        y=current.y + delta.y,
        z=current.z + delta.z
    }
    table.insert(move_data.move_stack, dir) 
    table.insert(move_data.pos_stack, new_pos)
    move_data.cur_pos = new_pos   
end

return move_data